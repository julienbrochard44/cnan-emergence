World world;

float agentDiameter = 3;
float mergeDistance = 1;
float movePercent = 0.025;
int numberOfAgent = 2500;
boolean debugger = true;

void settings() {
  size(800, 500);
  
}

void setup() {
  frameRate(60);
  world = new World(numberOfAgent);
}

void draw() {
  background(200, 200, 200);
  world.update();
  world.draw();
}

class Agent {
  private PVector pos;
  private float diameter;
  private color colour;
  private boolean deleted = false;
  private Agent nearestAgent = null;
  
  public Agent(float x, float y, float d) {
    this.pos = new PVector(x, y);
    this.colour = color(random(255), random(255), random(255));
    this. diameter = d;
  }
  
  public void setDeleted() {
    this.deleted = true;
  }
  public boolean isDeleted() {
    return this.deleted;
  }
  public PVector getPos() {
    return this.pos;
  }
  public color getColor() {
    return this.colour;
  }
  public float getDiameter() {
    return this.diameter;
  }
  public float getRadius() {
    return this.diameter/2;
  }

  public float getArea() {
    return PI*sq(this.getRadius());
  }
  
  public void move() {
    if(this.nearestAgent != null) {
      //this.pos.add(PVector.fromAngle(atan2(this.nearestAgent.getPos().y - this.pos.y, this.nearestAgent.getPos().x - this.pos.x)));
      this.pos.add(PVector.sub(this.nearestAgent.getPos(), this.pos).mult(movePercent));
    }
  }
  
  
  public void draw() {
    rectMode(CENTER);
    fill(this.colour);
    stroke(this.colour);
    circle(this.pos.x, this.pos.y, this.diameter);
    
    
    if(debugger && this.nearestAgent != null) {
      stroke(0, 0, 255);
      line(this.pos.x, this.pos.y, this.nearestAgent.getPos().x, this.nearestAgent.getPos().y);
    }
  }
  
  public void nearestAgent(ArrayList<Agent> agents) {
    
    // More than the maximum distance possible
    float dist = height*width;
    
    // Return null if none were found
    Agent found = null;
    
    // Get the closer Agent and its distance
    for(Agent a : agents) {
      if(this != a && this.pos.dist(a.getPos()) < dist) {
        dist = this.pos.dist(a.getPos());
        found = a;
      }
    }
    this.nearestAgent = found;
  }
  
  public void merge(ArrayList<Agent> agents) {
    // If the Agent didn't already merge and got deleted
    if(this.deleted == false) {
      
      for(Agent a : agents) {
        // If it's not itself and the other Agent is at a mergeDistance
        
        if(this != a && !this.isDeleted() && this.pos.dist(a.getPos()) < mergeDistance) {
          
          // Set this Agent color as the average between the other Agent's color and this Agent color
          this.colour = color( ((red(this.colour) + red(this.nearestAgent.getColor())) /2), ((green(this.colour) + green(this.nearestAgent.getColor())) /2), ((blue(this.colour) + blue(this.nearestAgent.getColor())) /2) );
          
          // Set this Agent diameter as the average between the other Agent's area and this Agent area
          this.diameter = sqrt( (this.getArea()+a.getArea()) /PI ) * 2 ;
          
          // Set the other Agent as deleted
          a.setDeleted();
        }
        
      }
      
    }
  }
}

class World {
  private ArrayList<Agent> agents = new ArrayList<Agent>();
  
  public World(int nb) {
    for(int i = 1; i <= nb; i++) {
      this.newAgent(random(width), random(height), agentDiameter);
    }
  }
  
  public void newAgent(float x, float y, float d) {
    this.agents.add(new Agent(x, y, d));
  }
  
  public void draw() {
    for(Agent a : this.agents) {
      a.draw();
    }
  }
  
  public void update() {
    
    // Create Agents on mouse press
    if(mousePressed) {
      this.newAgent(mouseX, mouseY, agentDiameter);
    }
    
    // Delete agents who merged
    this.deleteAgents();
    
    for(Agent a : this.agents) {
      
      // Update each Agent's nearest Agent
      a.nearestAgent(agents);
      
      // Move Agents
      a.move();
      
      // Merge Agents
      a.merge(this.agents);
    }
  }
  
  public void deleteAgents() {
    // Use a copy of the Agent list to parcour it and delete from the original list agents tagged as deleted
    ArrayList<Agent> copy = new ArrayList<Agent>(this.agents);
    for(Agent a : copy) {
      if(a.isDeleted()) {
        this.agents.remove(a);
      }
    }
  }
}

/*
void mouseClicked() {
  // Create a new Agent where the mouse was clicked
  world.newAgent(mouseX, mouseY);
}
*/
