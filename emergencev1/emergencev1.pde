World world;

void settings() {
  size(500, 300);
}

void setup() {
  world = new World(100);
}

void draw() {
  background(255, 255, 255);
  world.update();
  world.draw();
  /*if(mousePressed) {world.pair();}*/
}

class Agent {
  private PVector pos;
  private float diameter;
  private boolean paired = false;
  private Agent pair = null;
  private boolean deleted = false;
  
  public Agent() {
    this.pos = new PVector(random(width), random(height));
    this. diameter = 3;
  }
  
  public void setDeleted() {
    this.deleted = true;
  }
  public boolean isDeleted() {
    return this.deleted;
  }
  public PVector getPos() {
    return this.pos;
  }
  public float getDiameter() {
    return this.diameter;
  }
  public float getRadius() {
    return this.diameter/2;
  }
  public boolean isPaired() {
    return this.paired;
  }
  public void setPaired(boolean b) {
    this.paired = b;
  }
  public void setPair(Agent a) {
    this.pair = a;
  }
  public Agent getPair() {
    if(this.isPaired()) {
      return this.pair;
    }
    return null;
  }
  public float getArea() {
    return PI*sq(this.getRadius());
  }
  
  public void move() {
    if(this.paired) {
      this.pos.add(PVector.fromAngle(atan2(this.pair.getPos().y - this.pos.y, this.pair.getPos().x - this.pos.x)));
      //this.pos.add(PVector.fromAngle(PVector.angleBetween(this.pair.getPos(), this.pos)));
      //this.pos.add(this.pos.sub(this.pair.getPos()).normalize());
      //this.pos.add(this.pair.getPos().sub(this.pos).normalize());
    }
  }
  
  
  public void draw() {
    fill(0, 255, 0);
    stroke(0, 255, 0);
    circle(this.pos.x, this.pos.y, this.diameter);
    
    
    if(this.paired) {
      stroke(0, 0, 255);
      line(this.pos.x, this.pos.y, this.pair.getPos().x, this.pair.getPos().y);
    }
  }
  
  public void pair(ArrayList<Agent> agents) {
    if(this.paired == false) {
      float dist = height*width; // max dist
      Agent found = null;
      for(Agent a : agents) {
        if(this.pos.dist(a.getPos()) < dist && !a.isPaired() ) {
          dist = this.pos.dist(a.getPos());
          found = a;
        }
      }
      if(found != null) {
        this.setPaired(true);
        this.setPair(found);
        found.setPaired(true);
        found.setPair(this);
      }
      else {
        System.out.println("NO PAIR FOUND");
      }
    }
  }
  
  public void merge(ArrayList<Agent> agents) {
    if(this.deleted == false) {
      ArrayList<Agent> update = new ArrayList<Agent>(agents);
      update.remove(this);
      for(Agent a : update) {
          if(this.pos.dist(a.getPos()) < this.getRadius() + a.getRadius()) {
            
            this.diameter = sqrt( (this.getArea()+a.getArea()) /PI ) * 2 ;
            
            if(a.isPaired()) {
              a.getPair().setPair(null);
              a.getPair().setPaired(false);
              a.setPaired(false);
              a.setPair(null);
            }
            a.setDeleted();
            
            if(this.paired == true) {
              this.pair.setPaired(false);
              this.pair.setPair(null);
              this.setPaired(false);
              this.setPair(null);
            }
          }
      }
    }
  }
}

class World {
  private ArrayList<Agent> agents = new ArrayList<Agent>();
  
  public World(int nb) {
    for(int i = 1; i <= nb; i++) {
      this.agents.add(new Agent());
    }
  }
  
  public void draw() {
    for(Agent a : this.agents) {
      a.draw();
    }
  }
  
  public void update() {
    this.deleteAgents();
    this.pair();
    for(Agent a : this.agents) {
      a.move();
      a.merge(this.agents);
    }
  }
  
  public void deleteAgents() {
    ArrayList<Agent> copy = new ArrayList<Agent>(this.agents);
    for(Agent a : copy) {
      if(a.isDeleted()) {
        this.agents.remove(a);
      }
    }
  }
  
  public void pair() {
    ArrayList<Agent> copy = new ArrayList<Agent>(this.agents);
    for(Agent a : this.agents) {
      copy.remove(a);
      a.pair(copy);
    }
  }
  
}
